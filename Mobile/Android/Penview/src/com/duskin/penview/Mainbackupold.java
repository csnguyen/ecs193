package com.duskin.penview;

import android.R.color;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.RenderPriority;
import android.webkit.WebView;
import android.webkit.WebViewClient;



public class Mainbackupold extends Activity {

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		android.app.ActionBar bar = getActionBar();
		bar.hide();
		super.onCreate(savedInstanceState);

		
		setContentView(R.layout.activity_main);

		WebView webView1 = (WebView) findViewById(R.id.webView1);
		
		
		
		webView1.setWebChromeClient(new WebChromeClient()); 
		webView1.setWebViewClient(new WebViewClient());
		WebSettings settings = webView1.getSettings();
		
		//webView1.getSettings().setGeolocationDatabasePath( context.getFilesDir().getPath() );
		
		settings.setJavaScriptEnabled(true);
		settings.setRenderPriority(RenderPriority.HIGH);
		settings.setPluginState(android.webkit.WebSettings.PluginState.ON_DEMAND);
		settings.setAppCacheEnabled(true);
		settings.setDatabaseEnabled(true);
		settings.setDomStorageEnabled(true);
		settings.setGeolocationEnabled(true);
        webView1.setBackgroundColor(0);
        webView1.setBackgroundResource(color.black);
		//webView1.loadUrl("file:///android_asset/51514/index.html");
        webView1.loadUrl("http://ec2-54-187-98-99.us-west-2.compute.amazonaws.com/penguins/");
        //webView1.loadUrl("file:///android_asset/www/index.html");
		forceWebViewRedraw(webView1);

		


		
		//if (savedInstanceState == null) {
			//getSupportFragmentManager().beginTransaction()
		//			.add(R.id.container, new PlaceholderFragment()).commit();
		//}
	}

	private void forceWebViewRedraw(final WebView mWebView)
	{
	    mWebView.post(new Runnable() {
	        @Override
	        public void run()
	        {
	            mWebView.invalidate();
	            if(!isFinishing())
	                mWebView.postDelayed(this, 1000);
	        }
	    });
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);

			
			return rootView;
		}
	}

}
