/* Responsive HTML5 gallery tutorial
 *
 * http://tomicloud.com/2014/01/responsive-gallery
 * https://github.com/tomimick/responsive-image-gallery */
var BASE_URL = "backend/API/";
var isSaving = false;
$(
		function main() {

			var galCount = 0;
			var curr_count = 0;
			var local_search = 0;
			var imageGalleryInit = 0;
			var latitude = 0;
			var longitude = 0;
			var bool_location_exists = 0;
			var var_lat = '';
			var var_long = '';
			var penguinIDS = [];
			var plusSignExists = 0;
			var fetch = false;
			var menu = $("#menu");
			var main = $("#main");
			var top = $("#top");
			var home = $("#home");
			var about = $("#about");
			var images = $("#images");
			var drawingCanvas = $("#drawingCanvas");
			var favorites = $("#favorites");
			var carousel = $("#carousel");
			var topsearch = $("#topsearch");
			var is_favorites_active = false;
			var already_inside_gallery = 0;
			var carousel_obj = new Carousel("#carousel");
			var same_view = 0;
			var penName = "";
			var visible = false;
			var in_tutorial = false;
			var next = 0;
			var chosen = 0;

			// remove 300ms click delay in mobile browsers
			FastClick.attach(document.body);

			// associate all click handlers
			set_click_handlers();
			location.hash = "";

			// clone menu hierarchy for large screen
			$("#top").append(menu.find("ul").clone());

			// url hash has changed
			$(window).on('hashchange', function() {
				route_url();
			});

			/**
			 * -1: Unknown
				-2: Database Error
				-3: Filesystem Error
				-4: Error with the format or existence of parameters
				-5: Image processing error
				-6: IP error, usually flood control of some sort.
			 */
			function handleApiError(errorCode, data)	{
				var msg = "";
				var tl = "";
				switch(errorCode)	{
					case "-1":
						tl = "Unknown Error";
						msg = "Unknown Error!!: " + data;
						break;
					case "-2":
						tl = "Database Error";
						msg = "There was an error contacting the database!";
						break;
					case "-3":
						tl = "File Error";
						msg = "There was an error with the filesystem!";
						break;
					case "-4":
						tl = "Parameter Error";
						msg = "Something was wrong with your request! Try again!";
						break;
					case "-5":
						tl = "Image Processing Error";
						msg = "Something went wrong processing your penguin! Try again!";
						break;
					case "-6":
						tl = "Flood Error";
						msg = "Slow down, you're performing too many requests! Try again in a few seconds!";
						break;
					default:
				}
				$.growl.error({ title: tl, message: msg});
			}

			// route to main page
			route_url();

			// resize handler
			$(window).on('resize orientationchange', function() {
				resize_images();
			});

			navigator.geolocation.getCurrentPosition(success, error);

			if (!navigator.geolocation) {
				$.growl.error({ message: "Geolocation is not supported by your browser." });
				return;
			}

			function success(position) {
				latitude = position.coords.latitude;
				longitude = position.coords.longitude;
				var_lat = '' + latitude;
				var_long = '' + longitude;
			};

			function error() {
				$.growl.error({ message: "Unable to retrieve your location." });
			}
			;

			// based on url hash, adjust layout
			function route_url() {
				var hash = window.location.hash;
				var fade_carousel = true;
				is_favorites_active = false;

				// select active menu item
				$("ul.menu").find("a").removeClass("active");
				var active = $("ul.menu").find(
						"[href='" + (hash ? hash : "#") + "']");
				active.addClass("active");
				top.find("h1 a").text(active.eq(0).text());

				if (!hash) {
					home.show();
					drawingCanvas.hide();
					$("#imageGallery").hide();
					favorites.hide();
					about.hide();
					top.hide();
					$("#topsearch, #main").removeClass("opensearch");
				} else if (hash == "#gallery") {
					in_tutorial = 0;
					next = 0;
					if (same_view == 0) {
						clearCanvas();
						$(".ico").attr("href", "#gallery");
						top.find("h1 a").attr("href", "#gallery");
						$("#localbut").show();
						$("#randombut").show();
						$("#toolsbut").hide();
						$("#savebut").hide();
						$("#searchbut").show();
						$("#images").empty();
						penguinIDS = [];
						plusSignExists = 0;
						galCount=0;
						home.hide();
						drawingCanvas.hide();
						fetch_images();
						$("#imageGallery").show();
						images.show();
						visible = false;
						about.hide();
						favorites.hide();
						top.show();
						resize_images();
					} else if (same_view == 1) {
						same_view = 0;
						images.show();
					}
				} else if (hash == "#drawingCanvas") {
					already_inside_gallery = 0;
					$(".ico").attr("href", "#drawingCanvas");
					top.find("h1 a").attr("href", "#drawingCanvas");
					$("#localbut").hide();
					$("#randombut").hide();
					$("#toolsbut").hide();
					$("#paintTools").show();
					$("#savebut").show();
					$("#searchbut").hide();
					$("#topsearch, #main").removeClass("opensearch");
					home.hide();
					drawingCanvas.show();
					$("#imageGallery").hide();
					images.show();
					visible = false;
					about.hide();
					favorites.hide();
					top.show();
					penguinFade();
					resizeCanvas();
					if(in_tutorial){
						tutorial_tour();
					}
				} else if (hash == "#about") {
					in_tutorial = 0;
					next = 0;
					already_inside_gallery = 0;
					clearCanvas();
					$(".ico").attr("href", "#about");
					top.find("h1 a").attr("href", "#about");
					$("#localbut").hide();
					$("#randombut").hide();
					$("#savebut").hide();
					$("#searchbut").hide();
					$("#toolsbut").hide();
					$("#topsearch, #main").removeClass("opensearch");
					home.hide();
					drawingCanvas.hide();
					$("#imageGallery").hide();
					visible = false;
					images.show();
					about.show();
					favorites.hide();
					top.show();
				} else if (hash == "#favorites") {
				} else if (hash.slice(0, 5) == "#view") {
					// view image at index
					var i = parseInt(hash.slice(6));
					var div = $("#images").find(">div").eq(i);
					show_carousel(div);
					about.hide();
					favorites.hide();
					fade_carousel = false;
				}

				if (fade_carousel && carousel.hasClass("anim2"))
					hide_carousel();
			}

			// Sets paintbrush strokes relative to how they would look unchanged
			// on a
			// square canvas of width 500
			// If the smaller dimension is greater than 500, don't upscale the
			// paintbrush size

			function resizeStroke(x) {
				if (Math.min(oCtx.canvas.width, oCtx.canvas.height) >= 500)
					return x;
				return (Math.min(oCtx.canvas.width, oCtx.canvas.height) / 500 * x);
			}

			// sets all click handlers
			function set_click_handlers() {
				$("#show_gallery").click(function() {
					clearCanvas(); // clearing the canvas
					window.location.hash = "#gallery";
				});

				$("#gallery_show")
				.click(
						function() {
							clearCanvas(); // clearing the canvas
						
							if (already_inside_gallery == 2) {
								visible = false;
								reload();

							} else {
								window.location.hash = "#gallery";
							}
						});

				$("#show_canvas").click(function() {
					clearCanvas(); // clearing the canvas
					window.location.hash = "#drawingCanvas";
				});

				$("#show_about").click(function() {
					clearCanvas(); // clearing the canvas
					window.location.hash = "#about";
					$("ul.menu").find("a").removeClass("active");
					var active = $("ul.menu").find("[href='#about']");
					active.addClass("active");
					top.find("h1 a").text(active.eq(0).text());
					$(".ico").attr("href", "#about");
					$("#localbut").hide();
					$("#randombut").hide();
					$("#savebut").hide();
					$("#toolsbut").hide();
					home.hide();
					$("#imageGallery").hide();
					visible = false;
					//images.hide();
					drawingCanvas.hide();
					about.show();
					top.show();
				});

				$("#show_tutorial").click(function(){
					clearCanvas(); // clearing the canvas
					in_tutorial = true;
					window.location.hash = "#drawingCanvas";
				});

				$("#size5").click(function() {
					if(!in_tutorial || next == 1 || next == 3){
						lineWidth = resizeStroke(5);
						selectSize("size5");
						if(in_tutorial && next == 1){
							chosen = 1;
							vex.dialog.confirm({message: "Great! Now try painting on the canvas."});
						}
					}else if(in_tutorial){
						vex.dialog.confirm({message: "Please follow the tutorial."});
					}
				});
				$("#size10").click(function() {
					if(!in_tutorial || next == 1 || next == 3){
						lineWidth = resizeStroke(10);
						selectSize("size10");
						if(in_tutorial && next == 1){
							chosen = 1;
							vex.dialog.confirm({message: "Great! Now try painting on the canvas."});
						}
					}else if(in_tutorial){
						vex.dialog.confirm({message: "Please follow the tutorial."});
					}
				});
				$("#size20").click(function() {
					if(!in_tutorial || next == 1 || next == 3){
						lineWidth = resizeStroke(20);
						selectSize("size20");
						if(in_tutorial && next == 1){
							chosen = 1;
							vex.dialog.confirm({message: "Great! Now try painting on the canvas."});
						}
					}else if(in_tutorial){
						vex.dialog.confirm({message: "Please follow the tutorial."});
					}
				});
				$("#size30").click(function() {
					if(!in_tutorial || next == 1 || next == 3){
						lineWidth = resizeStroke(30);
						selectSize("size30");
						if(in_tutorial && next == 1){
							chosen = 1;
							vex.dialog.confirm({message: "Great! Now try painting on the canvas."});
						}
					}else if(in_tutorial){
						vex.dialog.confirm({message: "Please follow the tutorial."});
					}
				});
				$("#size40").click(function() {
					if(!in_tutorial || next == 1 || next == 3){
						lineWidth = resizeStroke(40);
						selectSize("size40");
						if(in_tutorial && next == 1){
							chosen = 1;
							vex.dialog.confirm({message: "Great! Now try painting on the canvas."});
						}
					}else if(in_tutorial){
						vex.dialog.confirm({message: "Please follow the tutorial."});
					}
				});
				$("#size50").click(function() {
					if(!in_tutorial || next == 1 || next == 3){
						lineWidth = resizeStroke(50);
						selectSize("size50");
						if(in_tutorial && next == 1){
							chosen = 1;
							vex.dialog.confirm({message: "Great! Now try painting on the canvas."});	
						}
					}else if(in_tutorial ){
						vex.dialog.confirm({message: "Please follow the tutorial."});
					}
				});
				$("#normal_paintbrush").click(function() {
					if(!in_tutorial || next == 2 || next == 3){
						paintbrush_type = 0;
						selectShadow("normal_paintbrush");
						if(in_tutorial && next == 2){
							chosen = 1;
							vex.dialog.confirm({message: "Great! Now try painting on the canvas."});
						}				
					}else if(in_tutorial){
						vex.dialog.confirm({message: "Please follow the tutorial."});
					}
				});
				$("#wc_paintbrush").click(function() {
					if(!in_tutorial || next == 2 || next == 3){
						paintbrush_type = 1;
						selectShadow("wc_paintbrush");
						if(in_tutorial && next == 2){
							chosen = 1;
							vex.dialog.confirm({message: "Great! Now try painting on the canvas."});
						}
					}else if(in_tutorial){
						vex.dialog.confirm({message: "Please follow the tutorial."});
					}
				});

				// small screen: open/hide sliding menu
				$("#menubut, #menu a").click(
						function() {
							$("#main,#menu,#top,#topsearch, #tools").toggleClass(
							"openmenu").removeClass("opensearch").removeClass("opentools");
							if ($(this).attr("id") == "menubut")
								return false;
						});

				$("#toolsbut").click(
						function() {
							if (window.innerWidth <= 480)	{
								$("#main,#menu,#top,#topsearch, #tools").toggleClass(
								"opentools").removeClass("opensearch").removeClass("openmenu");
								if ($(this).attr("id") == "toolsbut")
									return false;
							}
						});
						
						
						
				// reload all
				$("#reloadbut").click(function() {
					galCount = 0;
					reload();
				});

				$("#savebut").click(function() {
					if(!in_tutorial || next == 3){
						if (canvasIsDirty == true) {
							save();
						} else {
							$.growl.error({ message: "You can't save an empty canvas!" });
						}
						if(in_tutorial){
							in_tutorial = 0;
							next = 0;
							vex.dialog.confirm({message: "Great job on your first penguin! We hope you have fun drawing more. :) "});
						}
					}else if(in_tutorial){
						vex.dialog.confirm({ message: "Please follow the tutorial."});
					}
				});

				$("#localbut").click(function() {
					setTimeout(function() {
						$("#topsearch input").focus();
					}, 500);
					$("#topsearch").removeClass("opensearch");
					main.removeClass("opensearch");
					$("#images").empty();
					$("#images").show();
					visible = false;
					penguinIDS = [];
					galCount=0;
					plusSignExists = 0;
					local_search = 1;
					locateLocalPenguins();
					already_inside_gallery = 2;

				});

				$("#randombut").click(function() {
					setTimeout(function() {
						$("#topsearch input").focus();
					}, 500);
					$("#topsearch").removeClass("opensearch");
					main.removeClass("opensearch");
					$("#images").empty();
					$("#images").show();
					visible = false;
					penguinIDS = [];
					plusSignExists = 0;
					local_search = 0;
					galCount=0;
					fetch_images();
					already_inside_gallery = 2;
				});

	
				// open up search
				$("#searchbut").click(
						function() {
							$("#images").empty();
							$("#images").show();
							local_search = 2;
							penguinIDS = [];
							plusSignExists = 0;
							if (visible) {
								setTimeout(function() {
									$("#topsearch input").focus();
								}, 500);
								$("#topsearch").removeClass("opensearch");
								main.removeClass("opensearch");
								local_search = 0;
								galCount=0;
								fetch_images();
								already_inside_gallery = 2;
							} else {
								$("#topsearch input").val("");

								if ($("#topsearch").toggleClass("opensearch")
										.hasClass("opensearch"))
									setTimeout(function() {
										$("#topsearch input").focus();
									}, 500);
								main.toggleClass("opensearch");
								already_inside_gallery = 2;
							}	
							visible = !visible;
						});

				// click on image
				$("#images, #favorites").on(
						"click",
						"div",
						function(t) {
							var div = $(t.target);

							if (!div.is("div.my"))
								div = div.parents("div");
							if (!div.hasClass("my"))
								return;

							// build_carousel(is_favorites_active ? favorites :
							// images);
							build_carousel(images);

							if (div.hasClass("more")) {
								plusSignExists = 0;
								images.show();
								load_more(div);
							} else {
								same_view = 1;
								window.location.hash = "#view?" + div.index();
							}

						});

				// click on image area closes menu
				main.on("click", function() {
					if (main.hasClass("openmenu")) {
						$("#menubut").click();
						return false;
					}
				});

				// enter in search activates search
				$("#searchinput")
				.keyup(
						function(e) {
							if (e.keyCode == 13) {
								images.empty();
								penguinIDS = [];
								plusSignExists = 0;
								galCount=0;
								penName = document.getElementById("searchinput").value;
								searchByName(penName);
												// function to actually SEARCH for
												// penguins
							}
						});

				// escape always goes to home
				$(document.body).keyup(function(e) {
					if (e.keyCode == 27) {
						// hide_carousel();
						window.location.hash = "#gallery";
					}
				});

			}

			function tutorial_tour(){						
				vex.dialog.confirm({
				message : 'Welcome to the Tutorial Tour!',
				callback: function(value){
					if(value == true){
						tutorial_size();
					}else{
						tutorial_tour();
					}
				
				}
			});

			
			}

			function tutorial_size(){
				next = 1;
				vex.dialog.confirm({
				message: "You can use these paintbrush sizes on the left side of the toolbar. The default setting is medium, but you can change it. Try changing the size now!"});
			}

			function tutorial_type(){
				if(in_tutorial && next == 1 && chosen == 1){
					vex.dialog.confirm({message: "Good job! Now try choosing a paintbrush type. There is a water brush, and a normal brush.",
						callback: function(value){
							if(value){
								next = 2;
								chosen = 0;
							}
						}});
				}
				else if(in_tutorial && next == 2 && chosen == 1){
					vex.dialog.confirm({message: "Great, now finish drawing your penguin. Once you're done, click the little cloud on the top right to save your penguin. If you notice, the moment you stop painting your penguin, your penguin will start to Fade Away. Be sure to keep it alive long enough to save it!",
						callback: function(value){
							if(value){
								next = 3;
								chosen = 0;
							}
						}});
				}
				else if(in_tutorial && next == 3){

				}
				else if(in_tutorial && next == 1){
					clearCanvas();
					vex.dialog.confirm({message: "Please follow the tutorial."});
				}else if(in_tutorial){
					vex.dialog.confirm({message: "Please follow the tutorial."});
				}
			}
		
			function searchByName(penguinName){

				//STILL CANNOT SEARCH FOR MORE, AM JUST PUTTING THIS HERE SO IT DOESNT LOOK "BROKEN"
				// images.empty();
				// penguinIDS = [];
				// plusSignExists = 0;
				// alert("pressed search and got: " +
				// document.getElementById("searchinput").value);
				$
				.getJSON(
						BASE_URL
						+ "penguin"
						+ "?count=10&name="
						+ penguinName,
						function(data) {
							if (data.code == 0) {
								$.each(data.message,
										function(i, image) {
											if (penguinMatchID(data.message[i].id) == 0) {
											$("<div>").attr("class","my show")
													.attr("id","pengallery"+ galCount)
													.appendTo($("#images"));
											$("<img>")
											.attr(
													"class",
											"galleryImage")
											.attr(
													"id",
													data.message[i].id)
													.attr(
															"src",
															"backend/images/"
															+ data.message[i].image)
															.appendTo(
																	"#pengallery"
																	+ galCount);
											$("<div>").text(data.message[i].name)
													.appendTo("#pengallery"+ galCount)
													.css({
																'display' : 'block',
																'text-align' : 'center',
																'color' : 'white',
																'font-size' : '112%',
																'text-shadow' : '-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black'
															});
											galCount++;
											penguinIDS[data.message[i].id] = data.message[i].id;
										}
										});
								
								if (plusSignExists == 0) {
									images.append($("#morediv").clone()
											.removeAttr("id"));
									penguinIDS["morediv"] = "morediv";
									plusSignExists = 1;
								}
								resize_images();

							} else {
								$.growl.error({ message: "Sorry, there was an internal error. Please try again later." });
							}
							
						})
						.error(
								function() {
									console.log("Search by name");
									data = JSON.parse(data);
									console.log(data);
									handleApiError(data.code, data.message);
									
								})
			}

			// toggle favorite-status while in carousel
			function toggle_favorite(t) {
				// t.toggleClass("favorited");
				// var url = t.closest("li").find("img").attr("src");
				// var title = t.closest("li").find("div").html();

				// if (t.hasClass("favorited"))
				// add_favorite(url, title);
				// else
				// remove_favorite(url);
			}

			function toggle_flag(t) {
				var penguin_id = t.closest("li").find("img").attr("id");
				// var bool_flagged = false;
				if (t.hasClass("flagged")) {
					// should be posting FALSE to indicate UNFLAG
					$.post(BASE_URL + "flag/" + penguin_id, {
						message : false
					}, function(data) {
						data = $.parseJSON(data);
						t.removeClass("flagged");
					}).error(function(data) {
						console.log("Can't unflag this penguin");
						data = JSON.parse(data);
						console.log(data);
						handleApiError(data.code, data.message);
					});
				} else {
					// should be posting TRUE to indicate UNFLAG
					$
					.post(
							BASE_URL + "flag/" + penguin_id,
							{
								message : true
							},
							function(data) {
								data = $.parseJSON(data);
								t.addClass("flagged");
								if (data.code != 0) {
									$.growl.error({ message: "Sorry, there was an internal error. Can't flag this penguin. Please try again later." });
								}
							}).error(function() {
								console.log("Can't flag this penguin");
								data = JSON.parse(data);
								console.log(data);
								handleApiError(data.code, data.message);
							});
				}

			}

			// show carousel, position is at div
			function show_carousel(div) {
				carousel_obj.init();
				carousel.hide().removeClass("anim1 anim2");

				// initial image index
				var i = div.index();
				carousel_obj.showPane(i, false);

				// show zoom anim
				carousel.addClass("anim1");
				carousel.show();
				carousel.width(); // flush
				carousel.addClass("anim2");

				var penguin_id = div.find('img').attr('id');
				incrementViewCount(penguin_id);

				$(window).scrollTop(0);
			}

			function hide_carousel() {
				carousel.removeClass("anim2");

				carousel.one(
						"webkitTransitionEnd mozTransitionEnd transitionend",
						function() {
							// hide at end of transition
							carousel.hide();
						});
			}

			// resize images (width changed by CSS, height by this js)
			function resize_images() {
				var cont = is_favorites_active ? favorites : images;
				var w = cont.find("div.my").eq(0).width();
				cont.find("div.my").height(w);
			}

			// discard and reload all images
			function reload() {
				images.find("div").addClass("fadeaway");
				plusSignExists = 0;
				if (local_search == 1) {
					setTimeout(function() {
						images.empty();
						penguinIDS = [];
						locateLocalPenguins();
					}, 1000);
				} 
				else if(local_search == 2){
					setTimeout(function() {
						images.empty();
						penguinIDS = [];
						searchByName(penName);
					}, 1000);
				}
				else {
					setTimeout(function() {
						images.empty();
						penguinIDS = [];
						fetch_images();
					}, 1000);
				}
			}

			function locateLocalPenguins() {
				// search for local penguins
				$
				.getJSON(
						BASE_URL + "penguin" + "?count=10&lat="
						+ var_lat + "&long=" + var_long,
						function(data) {
							if (data.code == 0) {
								$.each(data.message,
										function(i, image) {
									if (penguinMatchID(data.message[i].id) == 0) {
												$("<div>")
												.attr("class","my show")
												.attr("id",
														"pengallery"
														+ galCount)
														.appendTo(
																$("#images"));
												$("<img>")
												.attr(
														"class",
												"galleryImage")
												.attr(
														"id",
														data.message[i].id)
														.attr(
																"src",
																"backend/images/"
																+ data.message[i].image)
																.appendTo(
																		"#pengallery"
																		+ galCount);
												$("<div>")
												.text(
														data.message[i].name)
														.appendTo("#pengallery"+ galCount)
																.css(
																		{
																			'display' : 'block',
																			'text-align' : 'center',
																			'color' : 'white',
																			'font-size' : '112%',
																			'text-shadow' : '-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black'
																		});
												galCount++;
												penguinIDS[data.message[i].id] = data.message[i].id;
									}
										});

								if (plusSignExists == 0) {
									images.append($("#morediv").clone()
											.removeAttr("id"));
									penguinIDS["morediv"] = "morediv";
									plusSignExists = 1;
								}
								resize_images();
							} else {
								$.growl.error({ message: "Sorry, there was an internal error. Please try again later." });
							}
						}).error(function() {
							console.log("Can't get local penguins");
							data = JSON.parse(data);
							console.log(data);
							handleApiError(data.code, data.message);
						});
			}

			function save() {
				// post penguin to backend
				// var result = prompt("Name your penguin");
				isSaving = true;
				var mydiv = document.getElementById('thecanvas');
				mydiv.style.opacity = 1;
				vex.dialog.prompt({
					message : 'What are you naming your penguin?',
					placeholder : 'Penguin name',
					callback : function(value) {
						if (!value)	{
							isSaving = false;
							return;
						}
						$.post(BASE_URL + "penguin",
								{
							lat : latitude,
							long : longitude,
							penguinImage : oCanvas
							.toDataURL("image/jpeg"),
							name : value.trim()
								}, function() {
									isSaving=false;
									clearCanvas();
									$.growl.success({ message: "Penguin has been saved!"});
								}).error(function() {
									console.log("Not creating penguin correctly.");
									data = JSON.parse(data);
									console.log(data);
									handleApiError(data.code, data.message);
									isSaving=false;
								});
					}
				});
			}

			// load more images
			function load_more(div) {
				if (div.find("i").hasClass("cg")) {

				} else {
					div.find("i").addClass("fa-refresh");
				}

				div.addClass("start");

				// can't use this since other can fire it
				// div.one("webkitTransitionEnd mozTransitionEnd transitionend",
				// function(){
				//if local_search, then find local penguins
				if (local_search == 1) {
					setTimeout(function() {
						if(!fetch)	{
							images.find(".more").remove();
							plusSignExists = 0;
						}
						locateLocalPenguins();
					}, 700);
				} 
				//if local_search is 2, then find searched penguins
				else if(local_search == 2){
					setTimeout(function() {
						if(!fetch)	{
							images.find(".more").remove();
							plusSignExists = 0;
						}

						searchByName(penName);
					}, 700);
				}
				//else, find random penguins
				else {
					setTimeout(function() {
						if(!fetch)	{
							images.find(".more").remove();
							plusSignExists = 0;
						}
						fetch_images();
					}, 700);
				}

			}

			// find matching penguinID
			function penguinMatchID(penguin_id) {
				if (penguinIDS[penguin_id] == penguin_id) {
					return 1;
				}
				return 0;
			}
			
			// fetch images from Flickr
			function fetch_images() {
				$.getJSON(
						BASE_URL + "penguin" + "?count=10",
						function(data) {
							fetch = true;
							if (data.code == 0) {
								$
								.each(
										data.message,
										function(i, image) {
											if (penguinMatchID(data.message[i].id) == 0) {
												$("<div>")
												.attr(
														"class",
												"my show")
												.attr(
														"id",
														"pengallery"
														+ galCount)
														.appendTo(
																$("#images"));
												$("<img>")
												.attr(
														"class",
												"galleryImage")
												.attr(
														"id",
														data.message[i].id)
														.attr(
																"src",
																"backend/images/"
																+ data.message[i].image)
																.appendTo(
																		"#pengallery"
																		+ galCount);
												$("<div>")
												.text(
														data.message[i].name)
														.appendTo(
																"#pengallery"
																+ galCount)
																.css(
																		{
																			'display' : 'block',
																			'text-align' : 'center',
																			'color' : 'white',
																			'font-size' : '112%',
																			'text-shadow' : '-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black'
																		});
												galCount++;
												penguinIDS[data.message[i].id] = data.message[i].id;
											}
										});

								if (plusSignExists == 0) {
									images.append($("#morediv").clone()
											.removeAttr("id"));
									penguinIDS["morediv"] = "morediv";
									plusSignExists = 1;
								}
								resize_images();
								fetch = false;
							} else {
								$.growl.error({ message: "Sorry, there was an internal error. Please try again later." });
								fetch = false;
							}
						}).error(function() {
							console.log("Can't fetch images");
							data = JSON.parse(data);
							console.log(data);
							handleApiError(data.code, data.message);
						})
			}

			// build carousel <ul> from images in home/favorites
			function build_carousel(container) {
				images.hide();
				var ul = carousel.find("ul");
				ul.empty();

				container
				.find("div.my img")
				.each(
						function(i, elem) {
							var t = $(elem).clone();
							// favorited?
							var f = "";
							var flag = "";
							var bool_flagged = false;

							ul.append(t);
							ul.find("img:last").wrap("<li/>");
							var li = ul.find("li:last");
							li.append("<div>"
									+ $(elem).parent().find("div")
									.html() + "</div>");
							li
							.find('div')
							.css(
									{
										'display' : 'block',
										'text-align' : 'center',
										'color' : 'white',
										'font-size' : '112%',
										'text-shadow' : '-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black'
									});

							$
							.getJSON(
									BASE_URL
									+ "flag/"
									+ $(elem)
									.attr("id"),
									function(data) {
										if (data.code == 0) {
											bool_flagged = data.message;


											var url = t.attr("src");
											if (is_favorite(url))
												f = "favorited";
											if (bool_flagged == true) {
												flag = "flagged";
											}

											/*
											 * XXX fix portrait pics... if (t.height() >
											 * t.width()) t.addClass("portrait");
											 */

											li.append("<i class='fa fa-3x fa-times "
													+ f + "'></i>");
											li.append("<i class='fa fa-3x fa-flag "
													+ flag + "'></i>");
											// li.append("<i class='fa fa-3x fa-star
											// "+f+"'></i>");

										}
									})
									.error(
											function() {
												console.log("Flag Status");
												data = JSON.parse(data);
												console.log(data);
												handleApiError(data.code, data.message);
											});

						});
			}

			// fill favorites div with data in localstorage
			function fill_favorites() {
				var data = getdata();

				favorites.empty();

				if (!data.length) {
					favorites.append($("#favoempty").clone());
					return;
				}

				for (var i = 0; i < data.length; i++) {
					var obj = data[i];

					var elem = $("<div class='my show favorite'><img src='"
							+ obj.url + "'/><div>" + obj.title + "</div>");
					favorites.append(elem);
				}
			}

			function add_favorite(url, title) {
				var data = getdata();
				data.push({
					"title" : title,
					"url" : url
				});
				localStorage.setItem("imgdata", JSON.stringify(data));

				fill_favorites();
			}

			function remove_favorite(url) {
				var data = getdata();
				for (var i = 0; i < data.length; i++) {
					var obj = data[i];
					if (url == obj.url) {
						data.splice(i, 1);
						break;
					}
				}

				localStorage.setItem("imgdata", JSON.stringify(data));

				fill_favorites();
			}

			function is_favorite(url) {
				var data = getdata();
				for (var i = 0; i < data.length; i++) {
					var obj = data[i];
					if (url == obj.url)
						return true;
				}
			}

			function incrementViewCount(penguinId) {
				$.getJSON(BASE_URL + "penguin/" + penguinId, function(data) {
				}).error(function(data) {
					console.log("Error incrementing view count");
					data = JSON.parse(data);
					console.log(data);
					handleApiError(data.code, data.message);
				});
			}

			// return array containing favorite objects
			function getdata() {
				var data = localStorage.getItem("imgdata");
				if (!data)
					return [];
				return JSON.parse(data);
			}

			/*--------------------------------------------------------------------------*/
			// carousel component from hammer.js demo
			// handle taps of <i> in the carousel
			function handle_tap(el) {
				if (!el.is("i"))
					// hide_carousel();
					// window.history.back();
					window.location.hash = "#gallery";
				else if (el.is(".fa-times"))
					// hide_carousel();
					window.location.hash = "#gallery";
				// window.history.back();
				else if (el.is(".fa-flag")) {
					toggle_flag(el);
				}

				// else if (el.is(".fa-star"))
				// toggle_favorite(el);
				else if (el.is(".fa-arrow-circle-o-left")) {
					carousel_obj.prev(el);
					incrementViewCount(el.parents().find("ul").find("img")
							.attr("id"));
				} else if (el.is(".fa-arrow-circle-o-right")) {
					carousel_obj.next();
					incrementViewCount(el.parents().find("ul").find("img")
							.attr("id"));
				}



			}

			/**
			 * super simple carousel animation between panes happens with css
			 * transitions
			 */
			function Carousel(element) {
				var self = this;
				element = $(element);

				var container = $(">ul", element);
				var panes = $(">ul>li", element);

				var pane_width = 0;
				var pane_count = panes.length;

				var current_pane = 0;

				/**
				 * initial
				 */
				this.init = function() {
					panes = $(">ul>li", element);
					pane_count = panes.length;

					setPaneDimensions();

					$(window).on("load resize orientationchange", function() {
						setPaneDimensions();
						// updateOffset();
					});
				};

				/**
				 * set the pane dimensions and scale the container
				 */
				function setPaneDimensions() {
					pane_width = element.width();
					panes.each(function() {
						$(this).width(pane_width);
					});
					container.width(pane_width * pane_count);
				}

				/**
				 * show pane by index
				 */
				this.showPane = function(index, animate) {
					// between the bounds
					index = Math.max(0, Math.min(index, pane_count - 1));
					current_pane = index;

					var offset = -((100 / pane_count) * current_pane);
					setContainerOffset(offset, animate);
				};

				function setContainerOffset(percent, animate) {
					container.removeClass("animate");

					if (animate) {
						container.addClass("animate");
					}

					if (Modernizr.csstransforms3d) {
						container.css("transform", "translate3d(" + percent
								+ "%,0,0) scale3d(1,1,1)");
					} else if (Modernizr.csstransforms) {
						container.css("transform", "translate(" + percent
								+ "%,0)");
					} else {
						var px = ((pane_width * pane_count) / 100) * percent;
						container.css("left", px + "px");
					}
				}

				this.next = function() {
					if(current_pane == pane_count - 1){
						return this.showPane(0, true);
					}
					return this.showPane(current_pane + 1, true);
				};
				this.prev = function() {
					if(current_pane == 0){
						return this.showPane(pane_count - 1, true);
					}
					return this.showPane(current_pane - 1, true);
				};

				function handleHammer(ev) {
					// disable browser scrolling
					if (ev.gesture)
						ev.gesture.preventDefault();

					switch (ev.type) {
					case 'dragright':
					case 'dragleft':
						// stick to the finger
						var pane_offset = -(100 / pane_count) * current_pane;
						var drag_offset = ((100 / pane_width) * ev.gesture.deltaX)
						/ pane_count;

						// slow down at the first and last pane
						if ((current_pane == 0 && ev.gesture.direction == "right")
								|| (current_pane == pane_count - 1 && ev.gesture.direction == "left")) {
							drag_offset *= .4;
						}

						setContainerOffset(drag_offset + pane_offset);
						break;

					case 'swipeleft':
						self.next();
						ev.gesture.stopDetect();
						break;

					case 'swiperight':
						self.prev();
						ev.gesture.stopDetect();
						break;

					case 'tap':
						handle_tap($(ev.target));
						break;

					case 'release':
						// more then 50% moved, navigate
						if (Math.abs(ev.gesture.deltaX) > pane_width / 2) {
							if (ev.gesture.direction == 'right') {
								self.prev();
							} else {
								self.next();
							}
						} else {
							self.showPane(current_pane, true);
						}
						break;
					default:
						break;
					}
				}

				var hammertime = new Hammer(element[0], {
					drag_lock_to_axis : true,
					drag_block_vertical : true
				});
				hammertime.on(
						"release dragleft dragright swipeleft swiperight tap",
						handleHammer);
			}


/*******READDING DRAWCANVAS.JS BACK INTO MAIN.JS BECAUSE OF TUTORIAL DIALOGS******/
/*--------------------------------------------------------------------------*/
			// canvas implementation
			//declaring variables

			var clickX = new Array();
			var clickY = new Array();
			var clickDrag = new Array();
			var lineWidths = new Array();
			var lineWidth = 0;
			var shadowBlurs = new Array();
			var bMouseIsDown = false; //mouse is down
			var canvasIsDirty = false;
			var stroke_img = new Image();

			var trailingBlur = 10;
			var waterColorBlur = 20;
			var topBarHeight = 60;	//This was causing the offset, the top bar was not accounted for in getting event Y coordinate
			var aspectWidth = 16;
			var aspectHeight = 9;


			var curr_count = 0;

			var canW = 0;
			var canH = 0;

			var oCanvas = document.getElementById("thecanvas");
			var oCtx = oCanvas.getContext('2d');

			var lastPoint;//used for stroke_paintbrush
			var timeInterval;//used for penguinFade()
			var iWidth = oCanvas.width;
			var iHeight = oCanvas.height;
			var paintbrush_type = 0;  //type of paintbrush
			var shadowBlur = 0;


			//setting up initial parameters
			oCtx.fillStyle = "rgb(255,255,255)";
			oCtx.fillRect(0,0,iWidth,iHeight);

			oCtx.strokeStyle = "rgb(0,0,0)";
			oCtx.lineJoin = oCtx.lineCap = 'round';
			oCtx.shadowBlur = shadowBlur;
			oCtx.shadowColor = 'rgb(0, 0, 0)';

			paintbrush_type = 0;
			shadowBlur = 0;

			window.addEventListener('resize', resizeCanvas, false);

			
			oCanvas.addEventListener("mousedown", mouseDown);
			oCanvas.addEventListener("mousemove", mouseMove);
			oCanvas.addEventListener("mouseup", mouseUp);
			oCanvas.addEventListener("mouseout", mouseOut);
			//oCanvas.addEventListener("mouseover", mouseOver);
			//oCanvas.addEventListener("mouseout", touchCancel);
			oCanvas.addEventListener("touchstart", touchDown);
			oCanvas.addEventListener("touchmove", touchMove);
			oCanvas.addEventListener("touchend", touchUp);
			oCanvas.addEventListener("touchcancel", touchCancel);


			//selecting the shadow to create the watercolor paint effect
			function selectShadow (id_of_paintbrush){
				if(paintbrush_type == 0)
					shadowBlur = 0;
				else if(paintbrush_type == 1)
					shadowBlur = 10;    
				else if(paintbrush_type == 2)
					shadowBlur = 0; 

				var elems = document.getElementsByTagName('*'), i;
				for (i in elems) {
					if((' ' + elems[i].className + ' ').indexOf(' ' + 'paintbrush' + ' ')
							> -1) {
						if(elems[i].getAttribute('id') == id_of_paintbrush){
							elems[i].setAttribute("style", "opacity: 1; display: inline; ");
						}else{
							elems[i].setAttribute("style", "opacity: 0.6; display: inline; ");
						}
					}
				}
			}

			//selecting the size of the paintbrush
			function selectSize(id_of_paintbrushSize){
				var elems = document.getElementsByTagName('*'), i;
				for (i in elems) {
					if((' ' + elems[i].className + ' ').indexOf(' ' + 'paintbrushSize' + ' ')
							> -1) {
						if(elems[i].getAttribute('id') == id_of_paintbrushSize){
							elems[i].setAttribute("style", "opacity: 1; display: inline;");
						}else{
							elems[i].setAttribute("style", "opacity: 0.6; display: inline");
						}
					}
				}
			}

			//when the mouse is down
			function mouseDown(e) {
				bMouseIsDown = true;
				canvasIsDirty = true;
				iLastX = e.offsetX || e.layerX;
				iLastY = e.offsetY || e.layerY;
				if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
					iLastX = e.pageX - oCanvas.offsetLeft;
					iLastY = e.pageY - oCanvas.offsetTop + document.body.scrollTop - topBarHeight;
				}

				addClick(iLastX, iLastY);
				draw();
				//oCtx.moveTo(iLastX, iLastY);
				lastPoint = {x: iLastX, y: iLastY};
			}

			//When touch begins
			function touchDown(e){
				e.preventDefault();
				bMouseIsDown = true;
				canvasIsDirty = true;
				
				iLastX = e.touches[0].clientX - oCanvas.offsetLeft + document.body.scrollLeft;
				iLastY = e.touches[0].clientY - oCanvas.offsetTop + document.body.scrollTop - topBarHeight;
				addClick(iLastX, iLastY);
				draw();
				lastPoint = {x: iLastX, y: iLastY};
			}

			//when the mouse is up
			function mouseUp(e) {
				bMouseIsDown = false;
				penguinFade();
				tutorial_type();

			}

			//When the finger is lifted
			function touchUp(e){
				bMouseIsDown = false;
				penguinFade();
				tutorial_type();
			}

			//if the mouse is down and being dragged
			function mouseMove(e) {
				if (bMouseIsDown && canvasIsDirty) {
					var iX = e.offsetX || e.layerX;
					var iY = e.offsetY || e.layerY;
					if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
						iX = e.pageX - oCanvas.offsetLeft;
						iY = e.pageY - oCanvas.offsetTop + document.body.scrollTop - topBarHeight;
					}
					
					// oCtx.moveTo(iLastX, iLastY);
					oCtx.lineTo(iX, iY);
					oCtx.stroke();
					iLastX = iX;
					iLastY = iY;
				}
			}

			function touchMove(e){
				e.preventDefault();
				if (bMouseIsDown && canvasIsDirty) {
				var iX = e.touches[0].clientX - oCanvas.offsetLeft + document.body.scrollLeft;
				var iY = e.touches[0].clientY - oCanvas.offsetTop + document.body.scrollTop - topBarHeight;

					oCtx.lineTo(iX, iY);
					oCtx.stroke();
					iLastX = iX;
					iLastY = iY;
				}
			}

			function touchCancel(e) {
				bMouseIdDown = false;
				penguinFade();
			}

			function mouseOut(e) {
				bMouseIsDown = false;
				penguinFade();
			}

			//add these to each of the arrays
			function addClick(x, y, dragging)
			{
				clickX.push(x);
				clickY.push(y);
				clickDrag.push(dragging);
				lineWidths.push(lineWidth);
				shadowBlurs.push(shadowBlur);
			}

			//drawing new element in the canvas 
			function draw(){
				var i = curr_count;
				oCtx.lineWidth = lineWidths[i]; 
				oCtx.shadowBlur = shadowBlurs[i];   
				oCtx.beginPath();
				if(clickDrag[i] && i){
					oCtx.moveTo(clickX[i-1], clickY[i-1]);
				}else{
					oCtx.moveTo(clickX[i]-1, clickY[i]);
				}
				oCtx.lineTo(clickX[i], clickY[i]);
				oCtx.closePath();
				oCtx.stroke();
				curr_count++;
			}

			//redrawing the canvas with the elements in the arrays
			//we actually can't redraw because of our drawing implementation
			//we cannot save all of our datapoints because we do not want
			//to slow down the app. Thus, if you resize, you will lose your drawing
			//instead of us keeping your drawing. However, this function is here in case
			//a new future implementation of draw is done, where every point is saved.
			function redraw(){
				oCtx.fillStyle = "rgb(255,255,255)";
				oCtx.fillRect(0,0,iWidth,iHeight);
				oCtx.strokeStyle = "rgb(0,0,0)";
				oCtx.lineJoin = oCtx.lineCap = 'round';
				oCtx.lineWidth = lineWidth;
				oCtx.shadowBlur = shadowBlur;
				oCtx.shadowColor = 'rgb(0, 0, 0)';
				for(var i=0; i < clickX.length; i++) {
					//var i = curr_count;
					oCtx.lineWidth = lineWidths[i]; 
					oCtx.shadowBlur = shadowBlurs[i];   
					oCtx.beginPath();
					if(clickDrag[i] && i){
						oCtx.moveTo(clickX[i-1], clickY[i-1]);
					}else{
						oCtx.moveTo(clickX[i]-1, clickY[i]);
					}
					oCtx.lineTo(clickX[i], clickY[i]);
					oCtx.closePath();
					oCtx.stroke();
					//curr_count++;
				}
			}

			//clearing the canvas
			function clearCanvas(){
			
				oCtx.fillStyle = "rgb(255,255,255)";
				oCtx.fillRect(0,0,oCanvas.width,oCanvas.height);
				oCtx.strokeStyle = "rgb(0,0,0)";
				oCtx.lineJoin = oCtx.lineCap = 'round';
				oCtx.lineWidth = lineWidth;
				oCtx.shadowBlur = shadowBlur;
				oCtx.shadowColor = 'rgb(0, 0, 0)';
				clickX = [];
				clickY = [];
				clickDrag = [];
				lineWidths = [];
				shadowBlurs = [];
				curr_count = 0;
				canvasIsDirty = false;
			}

			function resizeStroke(x) {
				if(Math.min(oCtx.canvas.width, oCtx.canvas.height) >= 500)
					return x;
				return (Math.min(oCtx.canvas.width, oCtx.canvas.height) / 500 * x);
				}

			function resizeCanvas() {
				$("#paintTools").removeClass("opentools");
			
				canW = window.innerWidth;
				canH = window.innerHeight - topBarHeight - window.innerHeight/100 - $("#paintTools").outerHeight(true) - 2*$("a.ico").css("padding").replace("px", ""); 
				
				a = Math.min(Math.floor(canH/aspectHeight), Math.floor(canW / aspectWidth));																						
				b = Math.min(Math.floor(canH/aspectWidth), Math.floor(canW/aspectHeight));
				if(b > a)
					{
					temp = aspectHeight;
					aspectHeight = aspectWidth;
					aspectWidth = temp;
					}

				if(canH/aspectHeight <= canW/aspectWidth){
					oCtx.canvas.height = Math.floor(canH / aspectHeight) * aspectHeight;
					oCtx.canvas.width = Math.floor(canH / aspectHeight) * aspectWidth;
					$("#topright").find("a").each(
						function(i, elem){ 
							var i_type = $(elem).find("i").attr("class");
							var id = $(elem).find("i").attr("id");
							if(id){
							$(elem).text("");
							$(elem).append("<i class=\"" + i_type + "\" id=\"" + id + "\"></i><b>" + id + "</b>");}
					});
				}
				else{
					oCtx.canvas.width = Math.floor(canW / aspectWidth) * aspectWidth;
					oCtx.canvas.height = Math.floor(canW / aspectWidth) * aspectHeight;
					$("#topright").find("a").each(
						function(i, elem){ 
							var i_type = $(elem).find("i").attr("class");
							var id = $(elem).find("i").attr("id");
							if(id){
							$(elem).text("");
							$(elem).append("<i class=\"" + i_type + "\" id=\"" + id + "\"></i>");}
						});
				}		

				
				
						document.getElementById("actualcanvasdiv").style.height = (oCtx.canvas.height).toString() + "px";
						document.getElementById("actualcanvasdiv").style.width = (oCtx.canvas.width).toString() + "px";
						document.getElementById("actualcanvasdiv").style.marginTop = (1).toString() + "%";
						document.getElementById("actualcanvasdiv").style.cssFloat="inherit";		


				selectShadow("normal_paintbrush");
				lineWidth = resizeStroke(30);
				oCtx.lineWidth = lineWidth;
				selectSize("size30");


				clearCanvas();
			}

			//creating the fade animation effect every 50 milliseconds
			function fadeTimeout()  {
				if (isSaving)	{
					return;
				}
				var mydiv = document.getElementById('thecanvas');
				var curr_opacity = window.getComputedStyle(mydiv,null).getPropertyValue("opacity");
				//if(!canvasIsDirty)
				//	return;
				if (bMouseIsDown != 0)  {
					mydiv.style.opacity = 1;
					return;
				}
				else if(curr_opacity < 0.002){
					clearCanvas();
					mydiv.style.opacity = 1;
					return;
				}
				else{
					curr_opacity = curr_opacity - 0.001;
				}
				mydiv.style.opacity = curr_opacity;
				setTimeout(fadeTimeout, 50);
			}

			//calling fadeAnimation every 2 seconds
			function penguinFade(){
				ptimeInterval = setTimeout(fadeAnimation, 2000);
			}

			//checking if mouse is down in order to call fadeTimeout
			function fadeAnimation(){
				if ((bMouseIsDown == 0)  && (canvasIsDirty) && !(isSaving)) {
					setTimeout(fadeTimeout,50);
				}
			}

		}).error(function() {
			console.log("ERROR! MAIN NOT LOADED")
		});