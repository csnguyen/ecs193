// setup our test canvas
// and a simple drawing function
window.onload = function() {

	// all these are click handlers for specific buttons
	document.getElementById("savepngbtn").onclick = function() {
		saveCanvas(oCanvas, "PNG");
	}
	document.getElementById("normal_paintbrush").onclick = function(){
		paintbrush_type = 0;
		selectShadow("normal_paintbrush");
	}
	document.getElementById("wc_paintbrush").onclick = function(){
		paintbrush_type = 1;
		selectShadow("wc_paintbrush");
	}
	// document.getElementById("stroke_paintbrush").onclick = function(){
	// 	paintbrush_type = 2;
	// 	draw("stroke_paintbrush");
	// }
	document.getElementById("size10").onclick = function(){
		lineWidth = 10;
		selectSize("size10");
	}
	document.getElementById("size20").onclick = function(){
		lineWidth = 20;
		selectSize("size20");
	}
	document.getElementById("size30").onclick = function(){
		lineWidth = 30;
		selectSize("size30");
	}
	document.getElementById("size40").onclick = function(){
		lineWidth = 40;
		selectSize("size40");
	}
	document.getElementById("size50").onclick = function(){
		lineWidth = 50;
		selectSize("size50");
	}
	document.getElementById("show_canvas").onclick = function(){
		var cTitle = document.getElementById("canvasTitle");
		cTitle.setAttribute("style", "display: none");
		var cDiv = document.getElementById("canvasDiv");
		cDiv.setAttribute("style", "display: inline");
		var a_Div =  document.getElementById("appDiv");
		a_Div.setAttribute("style", "background: white; overflow: none;");
		changeClassStyle("a_gallery", "display: none");
		changeClassStyle("paintbrush", "display: inline");
		changeClassStyle("paintbrushSize", "display:inline");
		//changeClassStyle("CanvasGalleryContainer", "top:9px; background: #993333; left: 20.35%; margin-left:0; height: 50px; width: 768px;");
		changeClassStyle("CanvasGalleryContainer", "top:9px; background: #993333; left: 5%; margin-left: 3px; height: 13px; width: 325px;");
		galleryHide();
	}
	document.getElementById("show_gallery").onclick = function(){
		var cTitle = document.getElementById("canvasTitle");
		cTitle.setAttribute("style", "display: none");
		var cDiv = document.getElementById("canvasDiv");
		cDiv.setAttribute("style", "display: none;");
		var a_Div =  document.getElementById("appDiv");
		a_Div.setAttribute("style", "overflow: auto; background: 0 -100px repeat-x url(https://mdn.mozillademos.org/files/5415/bg_gallery.png) #4F191A;");
		changeClassStyle("a_gallery", "display: block");
		changeClassStyle("paintbrush", "display: none");
		changeClassStyle("paintbrushSize", "display: none");
		//changeClassStyle("CanvasGalleryContainer", "top:9px; background: #993333; left: 20.35%; margin-left:0; height: 50px; width: 768px;");
		changeClassStyle("CanvasGalleryContainer", "top:9px; background: #993333; left: 5%; margin-left: 3px; height: 13px; width: 325px;");
		
		galleryDraw();
		
	}
	document.getElementById("clear_button").onclick = function(){
		clear();
	}

	
	
	//declaring variables
	var oCanvas = document.getElementById("thecanvas");
	var oCtx = oCanvas.getContext('2d');
	var clickX = new Array();
	var clickY = new Array();
	var clickDrag = new Array();
	var lineWidths = new Array();
	var shadowBlurs = new Array();
	var bMouseIsDown = false; //mouse is down
	var paintbrush_type = 0;  //type of paintbrush
	var stroke_img = new Image();
	stroke_img.src = 'stroke.png';
	//'http://www.tricedesigns.com/wp-content/uploads/2012/01/brush2.png';
	stroke_img.crossOrigin = 'anonymous';

	var lastPoint;//used for stroke_paintbrush
	var timeInterval;//used for penguinFade()
	var iWidth = oCanvas.width;
	var iHeight = oCanvas.height;
	var lineWidth = 50;
	var shadowBlur = 0;

	//setting up initial parameters
	oCtx.fillStyle = "rgb(255,255,255)";
	oCtx.fillRect(0,0,iWidth,iHeight);
	oCtx.strokeStyle = "rgb(0,0,0)";
	oCtx.lineJoin = oCtx.lineCap = 'round';
	oCtx.lineWidth = lineWidth;
	oCtx.shadowBlur = shadowBlur;
	oCtx.shadowColor = 'rgb(0, 0, 0)';

	oCanvas.addEventListener("mousedown", mouseDown);
    oCanvas.addEventListener("mousemove", mouseMove);
    oCanvas.addEventListener("mouseup", mouseUp);
	//oCanvas.addEventListener("mouseover", mouseOver);
	//oCanvas.addEventListener("mouseout", touchCancel);
    oCanvas.addEventListener("touchstart", touchDown);
    oCanvas.addEventListener("touchmove", touchMove);
    oCanvas.addEventListener("touchend", touchUp);
	oCavnas.addEventListener("touchcancel", touchCancel);

	
	
	// check if we have canvas support
	//var bHasCanvas = false;
	// if (oCanvas.getContext("2d")) {
	// 	bHasCanvas = true;
	// 	oCtx = oCanvas.getContext('2d');
	// }

	// // no canvas, bail out.
	// if (!bHasCanvas) {
	// 	return {
	// 		saveAsBMP : function(){},
	// 		saveAsPNG : function(){},
	// 		saveAsJPEG : function(){}
	// 	}
	// }

	//used to determine distance between two points
	//this is used for stroke_paintbrush
	function distanceBetween(point1, point2){
		return Math.sqrt(Math.pow(point2.x - point1.x, 2) + 
			Math.pow(point2.y - point1.y, 2));
	}

	//used to determine the angle between two points
	//this is used for stroke_paintbrush
	function angleBetween(point1, point2){
		return Math.atan2(point2.x - point1.x, point2.y-point1.y);
	}

	//selecting the shadow to create the watercolor paint effect
	function selectShadow (id_of_paintbrush){
		if(paintbrush_type == 0)
			shadowBlur = 0;
		else if(paintbrush_type == 1)
			shadowBlur = 10;	
		else if(paintbrush_type == 2)
			shadowBlur = 0;	

		var elems = document.getElementsByTagName('*'), i;
	    for (i in elems) {
	        if((' ' + elems[i].className + ' ').indexOf(' ' + 'paintbrush' + ' ')
	                > -1) {
	        	if(elems[i].getAttribute('id') == id_of_paintbrush){
	        		elems[i].setAttribute("style", "opacity: 1; display: inline; border: solid #666699 3px;");
	        	}else{
	        		elems[i].setAttribute("style", "opacity: 0.6; display: inline; border: solid #666699 3px;");
	        	}
	        }
	    }
	}

	//selecting the size of the paintbrush
	function selectSize(id_of_paintbrushSize){
		var elems = document.getElementsByTagName('*'), i;
	    for (i in elems) {
	        if((' ' + elems[i].className + ' ').indexOf(' ' + 'paintbrushSize' + ' ')
	                > -1) {
	        	if(elems[i].getAttribute('id') == id_of_paintbrushSize){
	        		elems[i].setAttribute("style", "opacity: 1; display: inline;");
	        	}else{
	        		elems[i].setAttribute("style", "opacity: 0.6; display: inline");
	        	}
	        }
	    }
	}

	//when the mouse is down
	function mouseDown(e) {
		e.preventDefault();
		bMouseIsDown = true;
		iLastX = e.clientX - oCanvas.offsetLeft + document.body.scrollLeft;
		iLastY = e.clientY - oCanvas.offsetTop + document.body.scrollTop;

		addClick(iLastX, iLastY);
		redraw();
		//oCtx.moveTo(iLastX, iLastY);
		lastPoint = {x: iLastX, y: iLastY};
	}

	//when the mouse is up
	function mouseUp(e) {
		e.preventDefault();
		bMouseIsDown = false;
	}

	function touchDown(e) {
		e.preventDefault();
		bMouseIsDown = true;
		iLastX = e.touches[0].clientX - oCanvas.offsetLeft + document.body.scrollLeft;
		iLastY = e.touches[0].clientY - oCanvas.offsetTop + document.body.scrollTop;

		addClick(iLastX, iLastY);
		redraw();
		//oCtx.moveTo(iLastX, iLastY);
		lastPoint = {x: iLastX, y: iLastY};
	}

	//when the mouse is up
	function touchUp(e) {
		e.preventDefault();
		bMouseIsDown = false;
	}

	
	function touchCancel(e){
	e.preventDefault();
	}
	
	//if the mouse is down and being dragged
	function mouseMove(e) {
		e.preventDefault();
		console.log("Move");
		if (bMouseIsDown) {
			var iX = e.clientX - oCanvas.offsetLeft + document.body.scrollLeft;
			var iY = e.clientY - oCanvas.offsetTop+ document.body.scrollTop;
			// oCtx.moveTo(iLastX, iLastY);
			if(paintbrush_type == 2){
				// var currentPoint = {x:iX, y:iY};
				// var dist = distanceBetween(lastPoint, currentPoint);
				// var angle = angleBetween(lastPoint, currentPoint);
				// for(var i = 0; i < dist; i++){
				// 	x = lastPoint.x + (Math.sin(angle)*i) - 25;
				// 	y = lastPoint.y + (Math.cos(angle)*i) - 25;
				// 	oCtx.drawImage(stroke_img, x, y);
				// }
				// lastPoint = currentPoint;
				// addClick(lastPoint.x, lastPoint.y, true);
				// redraw();
			}
			else{
				//oCtx.lineTo(iX, iY);
				//oCtx.stroke();
				//iLastX = iX;
				//iLastY = iY;
				addClick(iX, iY, true);
				redraw();
			}
		}
	}

	function touchMove(e) {
		e.preventDefault();
		console.log("MoveTOUCH");
		if (bMouseIsDown) {
			var iX = e.touches[0].clientX - oCanvas.offsetLeft + document.body.scrollLeft;
			var iY = e.touches[0].clientY - oCanvas.offsetTop+ document.body.scrollTop;
			// oCtx.moveTo(iLastX, iLastY);
			if(paintbrush_type == 2){
				// var currentPoint = {x:iX, y:iY};
				// var dist = distanceBetween(lastPoint, currentPoint);
				// var angle = angleBetween(lastPoint, currentPoint);
				// for(var i = 0; i < dist; i++){
				// 	x = lastPoint.x + (Math.sin(angle)*i) - 25;
				// 	y = lastPoint.y + (Math.cos(angle)*i) - 25;
				// 	oCtx.drawImage(stroke_img, x, y);
				// }
				// lastPoint = currentPoint;
				// addClick(lastPoint.x, lastPoint.y, true);
				// redraw();
			}
			else{
				oCtx.lineTo(iX, iY);
				oCtx.stroke();
				iLastX = iX;
				iLastY = iY;
				//addClick(iX, iY, true);
				//redraw();
			}
		}
	}
	
	
	
	//add these to each of the arrays
	function addClick(x, y, dragging)
	{
	  clickX.push(x);
	  clickY.push(y);
	  clickDrag.push(dragging);
	  lineWidths.push(lineWidth);
	  shadowBlurs.push(shadowBlur);
	  //clickColor.push(curColor);
	}

	//redrawing the canvas with the elements in the arrays
	function redraw(){
	  for(var i=0; i < clickX.length; i++) {
	  	oCtx.lineWidth = lineWidths[i];	
	  	oCtx.shadowBlur = shadowBlurs[i];	
	    oCtx.beginPath();
	    if(clickDrag[i] && i){
	      oCtx.moveTo(clickX[i-1], clickY[i-1]);
	     }else{
	       oCtx.moveTo(clickX[i]-1, clickY[i]);
	     }
	     oCtx.lineTo(clickX[i], clickY[i]);
	     oCtx.closePath();
	     oCtx.stroke();
	  }
	}

	//clearing the canvas
	function clear(){
		oCtx.fillStyle = "rgb(255,255,255)";
		oCtx.fillRect(0,0,oCanvas.width,oCanvas.height);
		clickX = [];
		clickY = [];
		clickDrag = [];
		lineWidths = [];
		shadowBlurs = [];

	}

	//changing a certain class style
	function changeClassStyle(matchClass, content) {
	    var elems = document.getElementsByTagName('*'), i;
	    for (i in elems) {
	        if((' ' + elems[i].className + ' ').indexOf(' ' + matchClass + ' ')
	                > -1) {
	            elems[i].setAttribute("style", content);
	        }
	    }
	}

	//hiding all the images in the gallery
	function galleryHide(){
		for (var i=0;i<document.images.length;i++){
			if(document.images[i].getAttribute('class') == 'gallery'){
				document.images[i].setAttribute("style","display: none");
			}
		}
		penguinFade();
	}

	//drawing all the images in the gallery
	function galleryDraw(){
		// Loop through all images
		  for (var i=0;i<document.images.length;i++){

		    // Don't add a canvas for the frame image
		    if (document.images[i].getAttribute('class')== 'gallery'){
		      document.images[i].setAttribute("style", "display: block");
			}
		  }
	}

	//creating the fade animation effect every 50 milliseconds
	function fadeTimeout()	{
		var mydiv = document.getElementById('thecanvas');
		var curr_opacity = window.getComputedStyle(mydiv,null).getPropertyValue("opacity");
		if (bMouseIsDown != 0)	{
			mydiv.style.opacity = 1;
			return;
		}
		else if(curr_opacity < 0.002){
			clear();
			mydiv.style.opacity = 1;
			return;
		}
		else{
				curr_opacity = curr_opacity - 0.001;
		}
		mydiv.style.opacity = curr_opacity;
		setTimeout(fadeTimeout, 50);
	}

	//calling fadeAnimation every 2 seconds
	function penguinFade(){
		timeInterval = setInterval(fadeAnimation, 2000);
	}

	//checking if mouse is down in order to call fadeTimeout
	function fadeAnimation(){
		if (bMouseIsDown == 0)	{
			setTimeout(fadeTimeout,50);
		}
	}

	//using ajax calls to pull up images from the server end.
	function doAjax() {
		$.getJSON(apiURL, function(data) {
				feedData = data;
				eventUpdate();
			});
	}

	function saveCanvas(pCanvas, strType) {
		var bRes = false;
		if (strType == "PNG"){
			bRes = Canvas2Image.saveAsPNG(oCanvas);//, 1, 1000, 1000);
		}
		if (strType == "BMP")
			bRes = Canvas2Image.saveAsBMP(oCanvas);
		if (strType == "JPEG")
			bRes = Canvas2Image.saveAsJPEG(oCanvas);

		if (!bRes) {
			alert("Sorry, this browser is not capable of saving " + strType + " files!");
			return false;
		}
	}
}


/*
 * Canvas2Image v0.1
 * Copyright (c) 2008 Jacob Seidelin, jseidelin@nihilogic.dk
 * MIT License [http://www.opensource.org/licenses/mit-license.php]
 */

var Canvas2Image = (function() {

	// check if we have canvas support
	var bHasCanvas = false;
	var oCanvas = document.createElement("canvas");
	if (oCanvas.getContext("2d")) {
		bHasCanvas = true;
	}

	// no canvas, bail out.
	if (!bHasCanvas) {
		return {
			saveAsBMP : function(){},
			saveAsPNG : function(){},
			saveAsJPEG : function(){}
		}
	}

	var bHasImageData = !!(oCanvas.getContext("2d").getImageData);
	var bHasDataURL = !!(oCanvas.toDataURL);
	var bHasBase64 = !!(window.btoa);

	var strDownloadMime = "image/octet-stream";

	var scaleCanvas = function(oCanvas, iWidth, iHeight) {
		if (iWidth && iHeight) {
			var oSaveCanvas = document.createElement("canvas");
			oSaveCanvas.width = iWidth;
			oSaveCanvas.height = iHeight;
			oSaveCanvas.style.width = iWidth+"px";
			oSaveCanvas.style.height = iHeight+"px";

			var oSaveCtx = oSaveCanvas.getContext("2d");

			oSaveCtx.drawImage(oCanvas, 0, 0, oCanvas.width, oCanvas.height, 0, 0, iWidth, iHeight);
			return oSaveCanvas;
		}
		return oCanvas;
	}

	return {

		saveAsPNG : function(oCanvas, bReturnImg, iWidth, iHeight) {
			if (!bHasDataURL) {
				console.log("bHasDataURL has no data");
				return false;
			}
			var oScaledCanvas = scaleCanvas(oCanvas, iWidth, iHeight);
			var strData = oScaledCanvas.toDataURL("image/png");
			if (bReturnImg) {
				//console.log("THERE WAS A BRETURNIMG AFTER ALL\n");
				//console.log(strData);
				return makeImageObject(strData);
			} else {
				//console.log("THERE WAS NO BRETURNIMG");
				var save_button = document.getElementById("savepngbtn");
				save_button.href = strData;
			//	saveFile(strData.replace("image/png", strDownloadMime));
			}
			return true;
		}//,

		// saveAsJPEG : function(oCanvas, bReturnImg, iWidth, iHeight) {
		// 	if (!bHasDataURL) {
		// 		return false;
		// 	}

		// 	var oScaledCanvas = scaleCanvas(oCanvas, iWidth, iHeight);
		// 	var strMime = "image/jpeg";
		// 	var strData = oScaledCanvas.toDataURL(strMime);
	
		// 	// check if browser actually supports jpeg by looking for the mime type in the data uri.
		// 	// if not, return false
		// 	if (strData.indexOf(strMime) != 5) {
		// 		return false;
		// 	}

		// 	if (bReturnImg) {
		// 		return makeImageObject(strData);
		// 	} else {
		// 		saveFile(strData.replace(strMime, strDownloadMime));
		// 	}
		// 	return true;
		// }

	};

})();
