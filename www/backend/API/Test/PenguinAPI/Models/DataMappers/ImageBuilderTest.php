<?php

use PenguinAPI\Models\DataMappers\ImageBuilder;
include(__DIR__ . '/../../../../vendor/autoload.php');
require_once __DIR__ . '/../../Data/Config.php';

class ImageBuilderTest extends \PHPUnit_Framework_TestCase	{

	protected $object;
	
	public function __construct()	{
		$this->object = new ImageBuilder();
	}
	
	public function testAll()	{
		$path = dirname(__FILE__);
		$images = $path . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR;
		$thumbs = $images . "thumbnails" . DIRECTORY_SEPARATOR;
		$testJpg = __DIR__ . '/../../Data/testPenguin.jpg';
		$hashSmall = md5_file(__DIR__ . '/../../Data/testThumb.jpg');
		$hs2 = 'c21bc9219c82fac357e74eb880f84342';
		$f = file_get_contents($testJpg);
		$hashBig = md5($f);
		$f = base64_encode($f);
		$this->object->setImage($f);
		$this->object->setPath($path );
		$this->object->processImage();
		$this->assertFileExists($thumbs . $hashBig . ".jpg");
		$this->assertFileExists($images . $hashBig . ".jpg");
		$testThumbHash = md5_file($thumbs . $hashBig . ".jpg");
		$this->assertTrue($hashSmall == $testThumbHash || $hs2 == $testThumbHash);
		$testImgHash = md5_file($images . $hashBig . ".jpg");
		$hI2 = '535d00afaeb1ac58da823e21400171b8';
		$this->assertTrue("7341d1ec6074806bedd66e9f23c41984" == $testImgHash || $hI2 == $testImgHash);
		unlink($thumbs . $hashBig . ".jpg");
		unlink($images . $hashBig . ".jpg");
		rmdir($thumbs);
		rmdir($images);
	}
	/**
     * @expectedException \Exception
     */
	public function testInvalidPath()	{
		$this->object->setPath("notapath");
	}
	
	/**
	 * @expectedException \Exception
	 */
	public function testInvalidImage()	{
		$this->object->setPath("notaimage");
	}

}
?>