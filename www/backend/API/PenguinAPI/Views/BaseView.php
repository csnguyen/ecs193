<?php

namespace PenguinAPI\Views;

abstract class BaseView	{
	protected $responseCode;
	protected $statusCode;
	protected $message;
	
	protected $template;
	
	protected $logger;
	
	/**
	 * Build the view, set default response parameters
	 * 
	 * @param Log $log
	 * @param Template $template
	 * @codeCoverageIgnore
	 */
	public final function __construct($log, $template)	{
		$this->logger = $log;
		$this->template = $template;
		$this->statusCode = 0;
		$this->responseCode = 200;
		$this->message = "";
	}
	/**
	 * Set the message variable in the response
	 * @param String $m
	 */
	public function setMessage($m)	{
		$this->message = $m;
	}
	/**
	 * This is the internal status code
	 * @param int $s
	 */
	public function setStatus($s)	{
		$this->statusCode = $s;
	}
	/**
	 * This sets the HTTP response code
	 * 
	 * @param int $r
	 */
	public function setResponse($r)	{
		$this->responseCode = $r;
	}
	/**
	 * Show the view after setting all the parameters
	 * 
	 * @codeCoverageIgnore
	 */
	public function render()	{
		http_response_code($this->responseCode);
		$out = $this->template->parse($this->statusCode,$this->message);
		$this->logger->setResp($out);
		echo $out;
	}
}