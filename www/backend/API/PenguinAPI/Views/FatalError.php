<?php
namespace PenguinAPI\Views;

class FatalError extends BaseView	{
	public function render()	{
		http_response_code($this->responseCode);
		$out = $this->template->parse($this->statusCode,$this->message);
		echo $out;
	}
}
?>