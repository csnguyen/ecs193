<?php

namespace PenguinAPI\Templates;

/**
 * Json view for the API
 * 
 * @author its-zach
 *
 */
class JSON extends BaseTemplate {
	
	/**
	 * Handles parsing the view variables into a json struct
	 * the $_GET['callback'] nastyness is for jsonp callbacks
	 */
	public function __construct()	{
		if (isset($_GET['callback'])) {
			$this->callback = $_GET['callback'];
			unset($_GET['callback']);
		}
	}
	/**
	 * Chews up a status code and response message and outputs json
	 * @param unknown $statusCode
	 * @param unknown $message
	 * @return string
	 */
	public function parse($statusCode, $message)	{
		$outArray = array(
				"code" => $statusCode,
				"message" => $message
		);

		$out = json_encode($outArray);
		
		if (isset($this->callback))	{
			return $this->callback . "(" . $out . ")";
		}
		else	{
			return $out;
		}
	}
}