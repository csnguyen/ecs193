<?php
namespace PenguinAPI\Models;

use PenguinAPI\Models\DB\DBWrapper;
use PenguinAPI\Exceptions\InvalidClassException;

class DataMapperFactory	{

	private $db;

	public function __construct($db)	{
		$this->db = $db;
	}

	/**
	 * Creates the data mapper object with the given name
	 * 
	 * @param unknown $className
	 * @return unknown
	 */
	public function get($className)	{
		$className = "PenguinAPI\\Models\\DataMappers\\" . $className;

		if (!class_exists($className)) throw new InvalidClassException(ERROR_GENERIC, $className . " not found", 500);

		return new $className($this->db);
	}

}

?>