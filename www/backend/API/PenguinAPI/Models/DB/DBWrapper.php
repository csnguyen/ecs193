<?php

namespace PenguinAPI\Models\DB;

/**
 * PDO Database implementation
 * 
 * @author its-zach
 * 
 *
 */
class DBWrapper implements DBInterface	{
	
	private $dbHandle;
	private $params;
	
	public function __construct()	{
		$this->dbHandle =  new \PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS);
		$this->dbHandle->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
		$this->params = array();
	}
	/**
	 * Adds a paramter to later be bound to a prepared request
	 * 
	 * @param unknown $key
	 * @param unknown $value
	 */
	public function addParam($key, $value)	{
		$this->params[$key] = $value;
	}
	
	/**
	 * Executes a Database Query
	 * 
	 * While you CAN concatenate variables into the request I HIGHLY recommend you don't (hardcoded table names from config notwithstanding)
	 * 
	 * Use addParam to bind variables to the statement prepared with this function
	 * 
	 * @param unknown $q
	 * @throws \PenguinAPI\Exceptions\DBException
	 * @return multitype:|unknown
	 */
	public function query($q)	{
		$stmt = $this->dbHandle->prepare($q);
		if ($stmt === false)	{
			throw new \PenguinAPI\Exceptions\DBException(ERROR_DB, "Unable to prepare statment. Error is: " . print_r($this->dbHandle->errorInfo(), true), 500);
		}
		$params =& $this->params;
		foreach($params as $key => &$value)	{
			$stmt->bindParam($key,$value);
		}
		if ($stmt->execute() == false) {
			return array();
		}

		$this->params = array();
		$out = @$stmt->fetchAll(\PDO::FETCH_ASSOC);
		return $out;
	}
	/**
	 * Helper function to grab the id of the last inserted row
	 * 
	 * @param unknown $id
	 * @return string
	 */
	public function lastInsertId($id)	{
		return $this->dbHandle->lastInsertId($id);
	}
}

?>