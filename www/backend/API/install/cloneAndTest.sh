rm -rf Backend
mkdir Backend
git clone git@bitbucket.org:csnguyen/ecs193.git
mv ./ecs193/Backend .
rm -rf ecs193
cd Backend/Penguin-RESTful-API-MVC
php composer.phar install
cd vendor
bin/phpunit -c ../Test/PHPUnit.xml ../Test/PenguinAPI/
cd ../ExternalTest
cp ../install/Config.default.php ../Data/Config.php
php ExternalTest.php
