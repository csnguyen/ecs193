<?php

use \PenguinAPI\Exceptions\ParameterException;
use \PenguinAPI\Models\DB\DBWrapper;
use \PenguinAPI\Utility\Log;
use \PenguinAPI\Utility\Router;
use \PenguinAPI\Models\ServiceFactory;
use \PenguinAPI\Models\DomainObjectFactory;
use \PenguinAPI\Models\DataMapperFactory;
use \PenguinAPI\Templates\JSON;
use \PenguinAPI\Controllers\NoRoute;
use \PenguinAPI\Views\BaseView;
use PenguinAPI\Views\FatalError;

require 'vendor/autoload.php';
require_once '../Config.php';

date_default_timezone_set ( "America/Los_Angeles");

if (!isset($_SERVER['REQUEST_METHOD']) ||	
	!isset($_SERVER['REQUEST_URI']) ||
	!isset($_SERVER['REMOTE_ADDR']))	{
	echo "Error: This is not meant to be run via CLI";
	exit(0);
}

//We only output via json, though we could do XML if we really wanted
$template = new JSON();

//Try to create DB and log instance

try	{
$db = new DBWrapper();

} catch (\Exception $e)	{
	$view = new FatalError(null, $template);
	$view->setResponse(500);
	$view->setMessage("Unable to connect to Database");
	$view->setStatus(ERROR_DB);
	$view->render();
	exit(0);
}
$log = new Log($db, DB_TABLE_LOG);

//Figure out the codepath we want to take
$route = new Router($log, file_get_contents("Data/Routes.json"));
$route->setPrefix(ROUTE_PREFIX);
$route->setRoute($_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI']);
unset($_GET['r']);

//Grab request parameters from globasl
if ($_SERVER['REQUEST_METHOD'] == "GET")
	$requestParams = $_GET;
else if ($_SERVER['REQUEST_METHOD'] == "POST")	{
	$requestParams = $_POST;
	//Grab the file, if it exists and put it in requestParams
}
else
	$requestParams = array();

$requestParams = array_merge($requestParams, $route->getParams());
//Throw IP in request params so we don't deal with global state anywhere else
$requestParams['requestIP'] = $_SERVER['REMOTE_ADDR'];

//Add parameters to log
$log->setParams($requestParams);

//Instantiate the serviceFactory
$serviceFactory = new ServiceFactory(
		new DomainObjectFactory(),
		new DataMapperFactory($db));

//Setup view and controller class definitions
$baseClass = $route->getClass();
$controllerType = "\\PenguinAPI\\Controllers\\".$baseClass;
$viewType = "\\PenguinAPI\\Views\\".$baseClass;

try	{
	//Instantiate view and controller
	$view = new $viewType($log,$template);
	$controller = new $controllerType($view, $serviceFactory, $requestParams);
	$controller->{$route->getMethod()}();
}
catch (Exception $e)	{
	//Something went wrong, the view or controller could not be instantiated
	$controller = new NoRoute(new BaseView($log, $template), $serviceFactory, $requestParams);
	$route->setRoute("", "");
	$controller->invoke();
}

//Display result, and log
$view->render();
$log->execute();