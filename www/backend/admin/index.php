<?php
require_once('../API/PenguinAPI/Models/DB/DBInterface.php');
require_once('../API/PenguinAPI/Models/DB/DBWrapper.php');
require_once('../Config.php');
require_once('./Utility/Helper.php');
require_once('./Utility/Template.php');

echo templateHeader();
$db = new \PenguinAPI\Models\DB\DBWrapper();

//If someone posted something to us, do something about it.
if (isset($_POST['delete'])) {
	$res = doAction($db,"Delete", $_POST['id']);
	echo templateAction("Delete", $_POST['id'], $res);
}

elseif (isset($_POST['clear']))	{
	$res = doAction($db,"Clear", $_POST['id']);
	echo templateAction("ignore", $_POST['id'], $res);
}

elseif (isset($_POST['unignore']))	{
	$res = doAction($db,"unignore", $_POST['id']);
	echo templateAction("unignore", $_POST['id'], $res);
}

//Initialize variables
$fCount = DEFAULT_FLAG_THRESHOLD;
//The threshold for how flagged a penguin needs to be to be displayed is set here
if (isset($_GET['fCount']))	
	$fCount = $_GET['fCount'];
if (!isset($_GET['ignored']))	{
	//Get the penguins who's flagCounts meet the threshold
	$out = getFlaggedPenguins($db,$fCount);
	
	//If we didn't find any, great, we're done (almost)
	if (count($out) == 0)	{
		echo templateNone();
	}
	else	{
		//Otherwise, we begin the table/form
		echo templateStart();
		foreach ($out as $peng)	{
			echo templateItem($peng, $fCount);
		}
		echo templateEnd();
	}
}
else	{
	$out = getIgnoredPenguins($db);
	
	//If we didn't find any, great, we're done (almost)
	if (count($out) == 0)	{
		echo templateNoneIg();
	}
	else	{
		//Otherwise, we begin the table/form
		echo templateStart();
		foreach ($out as $peng)	{
			echo templateItemIg($peng);
		}
		echo templateEnd();
	}
}


echo templateFooter();
?>