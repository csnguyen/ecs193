<?php

function getFlaggedPenguins($db, $fCount)	{
	$db->addParam(':fc', $fCount);
	$out = $db->query('SELECT id, image, name, flagCount from ' . DB_TABLE_PENGUIN . 
			' where flagCount >= :fc AND ignoreFlag = 0 ORDER BY flagCount DESC');

	return $out;
}

function getIgnoredPenguins($db)	{
	$out = $db->query('SELECT id, image, name, flagCount from ' . DB_TABLE_PENGUIN .
			' where ignoreFlag = 1 ORDER BY flagCount DESC');

	return $out;
}

function doAction($db,$action, $id)	{
	if ($action === "Delete")	{
		$o = deletePenguin($db,$id);
		if ($o === false)
			return false;
		return true;
	}
	else if ($action === "Clear")	{
		$o = clearFlags($db,$id);
		if ($o === false)
			return false;
		return true;
	}
	else if ($action === "unignore")	{
		$o = unignore($db,$id);
		if ($o === false)
			return false;
		return true;
	}
	else
		return false;
}

function deletePenguin($db,$id)	{
	$db->addParam(':id', $id);
	$db->query('DELETE FROM ' . DB_TABLE_GEO . ' WHERE id = :id');
	$db->addParam(':id', $id);
	return $db->query('DELETE FROM ' . DB_TABLE_PENGUIN . ' WHERE id = :id');	
}

function clearFlags($db,$id)	{
	$db->addParam(':id', $id);
	return $db->query('UPDATE ' . DB_TABLE_PENGUIN . ' SET ignoreFlag = 1 WHERE id = :id');
}

function unignore($db,$id)	{
	$db->addParam(':id', $id);
	return $db->query('UPDATE ' . DB_TABLE_PENGUIN . ' SET ignoreFlag = 0 WHERE id = :id');
}
?>