<?php
require_once('./Utility/Helper.php');
function templateAction($action, $id, $res)	{
	if ($res)	{
		$x = file_get_contents(dirname(__FILE__).'/../html/templateActionSuccess.html');
	}
	else{
		$x = file_get_contents(dirname(__FILE__).'/../html/templateActionFailure.html');
	}
	
	$x = str_replace(":ID", $id, $x);
	$x = str_replace(":ACTION", $action, $x);
	return $x;
}

function templateHeader()	{
	return file_get_contents(dirname(__FILE__).'/../html/header.html');
}

function templateNone()	{
	return file_get_contents(dirname(__FILE__).'/../html/templateNone.html');
}

function templateNoneIg()	{
	return file_get_contents(dirname(__FILE__).'/../html/templateNoneIg.html');
}

function templateStart()	{
	return file_get_contents(dirname(__FILE__).'/../html/templateStart.html');
}

function templateItem($peng, $fc)	{
	$row = file_get_contents(dirname(__FILE__).'/../html/templateRow.html');
	$row = str_replace(":ID", $peng['id'], $row);
	$row = str_replace(":FLAGS", $peng['flagCount'], $row);
	$row = str_replace(":NAME", $peng['name'], $row);
	$row = str_replace(":COUNT", $fc, $row);
	$url = IMAGE_URL . $peng['image'];
	$row = str_replace(":URL", $url, $row);
	return $row;
}

function templateItemIg($peng)	{
	$row = file_get_contents(dirname(__FILE__).'/../html/templateRowIgnored.html');
	$row = str_replace(":ID", $peng['id'], $row);
	$row = str_replace(":FLAGS", $peng['flagCount'], $row);
	$row = str_replace(":NAME", $peng['name'], $row);
	$url = IMAGE_URL . $peng['image'];
	$row = str_replace(":URL", $url, $row);
	return $row;
}

function templateEnd()	{
	return file_get_contents(dirname(__FILE__).'/../html/templateEnd.html');
}

function templateFooter()	{
	return file_get_contents(dirname(__FILE__).'/../html/footer.html');
}

?>